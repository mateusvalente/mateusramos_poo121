import java.util.Scanner; //Foi importado o pacote "java.util" para utiliza��o da classe "Scanner"

public class Exercicio01 {

	static final double PI = 3.14; //Declara��o de constante estatica global
	
	public static void main(String[] args) {
		
		//Declara��o e inicializa��o de vari�veis e constantes
		double raio, area;
		raio = 0;
		area = 0;
		
		Scanner tecla = new Scanner(System.in); //criando objeto "tecla" para entrada de dados
		
		//Entrada de dado
		System.out.println("Informe o raio do circulo:");
		raio = tecla.nextDouble();
		
		//Processamento de dados
		
		//area = 3.14 * Math.sqrt(raio);
		//area = Math.PI * Math.sqrt(raio);
		area = PI * Math.sqrt(raio); //Processamento pode ser feito de qualquer uma das formas acima. Dessa forma est� sendo utilizada a constante "PI", criada no inicio do c�digo.
		
		//Sa�da de informa��o
		System.out.println("A area do circulo �: " +area);
		

	}

}
